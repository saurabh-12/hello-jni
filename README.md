# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This is a demo hello-jni project to create .so file by using NDK.

### How do I get set up? ###
Just copy this folder in your machine where your NDK set-up belog(generally u dir in Ubuntu).
Then Setup NDK path in your terminal(Ubuntu).

* export PATH=$PATH:/u/android-ndk-r13b/
* SYSROOT=/u/android-ndk-r13b/platforms/android-21/arch-arm/
* After setting NDK path(as above), you go to jni dir
as for example /u/hello-jni/jni
then give ndk-build command. It will create .so file in lib folder in hello-jni folder.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact